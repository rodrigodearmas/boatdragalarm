---
hide:
    - toc
---

# MTPresencial
## MTPresencial - TRABAJO INTEGRADOR PRESENCIAL

***

## **<span style="color:red">Desafío</span>**  
#### **<span style="color:red">PRIMERA JORNADA</span>**  
1 - Formar equipos de trabajo (estudiantes presenciales y online) para realizar las siguientes tareas:   

* Montar una extrusor de pasta partiendo de una Anet A8.  
* Programar la placa de la Anet A8 con Arduino IDE
Modelar e imprimir un modelo en 3d con materiales biodegradables.  
* Realizar una presentación del trabajo realizado en los tres días.  
* Soldado de placas del módulo MT08.  

2 - Soldado de placas del módulo MT08.  

Documentar en equipo el  proceso y publicar en tu repositorio de gitlab.  

#### **<span style="color:red">PRIMERA JORNADA</span>**
**<span style="color:#71D16E"> EQUIPO DE TRABAJO.</span>**  

![](../images/TI/TI13.jpg)    

[Maximiliano Izzi](https://maximiliano_izzi.gitlab.io/pagina-maxi/about/about/) y [Rodrigo Dearmas](https://rodrigodearmas.gitlab.io/boatdragalarm/about/about/) de manera presencial y [Claudia Galanzino](https://claudia_galanzino.gitlab.io/claudiagalanzino/SobreMi/) en modalidad a distancia.  

***
## **<span style="color:#71D16E">Montar una extrusor de pasta partiendo de una Anet A8.</span>**  
[Marlin Firmware](https://github.com/MarlinFirmware/Marlin)  
[Impresora 3D Anet A8](https://anet3d.com/)   


**<span style="color:#71D16E">DESMONTAJE.</span>**  
![](../images/TI/TI06.jpg)    
Lo primero que se hizo fue sacar el extrusor. Para ello tuvimos que sacar la chapa negra que lo cubre, luego sacamos los tornillos del motor y el fancooler para que pueda desarmarse el extrusor y quedarnos con el motor paso a paso, que usaremos para mover la jeringa.

![](../images/TI/TI07.jpg)  

Luego comenzamos a armar la plataforma de plástico que irá en los rodamientos lineales y la base para la jeringa.  

![](../images/TI/TI10.jpg)  

Tenemos las piezas impresas para montar.    

![](../images/TI/TI09.jpg)  


Conjunto completo de piezas de Impresión 3d FDM (piezas para mejorar extrusor).

![](../images/TI/TI12.jpg)


**<span style="color:#71D16E">PROGRAMACION</span>**  
**Programar la placa de la Anet A8 con Arduino IDE/Programacion**  
Basado en el kit https://www.youmagine.com/designs/paste-extruder

* Descargamos e instalamos [Anet board](https://github.com/SkyNet3D/anet-board) en Arduino IDE.  

* Copiamos la carpeta hardware de …anet-board-master\anet-board-master y la pegamos en la carpeta Arduino.  


* Descargamos [Marlin y config v2.0.9.3](https://github.com/MarlinFirmware/Marlin)  

* Copiamos los archivos ubicados en la dirección …Configurations-release-2.0.9.3\config\examples\Anet\A8 y los pegamosen la dirección …Marlin-2.0.x\Marlin-2.0.x\Marlin.  

* En Arduino IDE, abrimos el archivo Marlin.ino
y editamos los parámetros en el archivo configuration.h. Seteos como deshabilitar la cama y deshabilitar la temperatura minima del extrusor.

* Compilamos, conectamos la impresora al usb y una vez seleccionado el puerto cargamos el programa pero nos encontramos con **errores de compilación**.  

![](../images/TI/TI11.jpg)  

* En este momento comenzamos a probar distintas opciones de solución, entre ellos:
  * probar de grabar un nuevo bootloader
  * probar de utilizar el arduino como isp
  * desconectar TODO
Nada de esto funcionó, por lo que haciendo caso a Mateo y compañía, decidimos conectar todo nuevamente y utilizarlo con el firmware original.

Para esto cambiamos el órden de los cables del paso a paso de manera que gire hacia el otro lado, conectamos el extrusor, termistor y cama caliente por lo que cada vez que enviamos a imprimir debemos setear los parámetros de temperatura de extrusión y cama.



**Pruebas de impresion en pasta de la primera jornada**  
Se realizo la preparación de una pasta para imprimir y se realizo la primera experimentación.  

![](../images/TI/TI14.jpg)  

https://materiom.org/
***

#### **<span style="color:red">SEGUNDA JORNADA</span>**
**<span style="color:#71D16E">Prusa Slicer</span>**  
Seteo y variacion de parametros en Prusa Slicer.
![](../images/TI/TI16.jpg)  

**Pruebas de impresion en pasta de la primera jornada**  
Se realizo la preparación de una pasta para imprimir y se realizo la primera experimentación.  

![](../images/TI/TI15.jpg)
![](../images/TI/TI19.jpg)
![](../images/TI/TI20.jpg)
![](../images/TI/TI21.jpg)



#### **<span style="color:red">TERCERA JORNADA</span>**
Soldado de placas de MT08.  

![](../images/TI/TI16.jpeg)
![](../images/TI/TI17.jpg)
![](../images/TI/TI18.mp4)

Presentaciones  





Proyecto de Tinkercad dónde se pueden editar los modelos del kit para las mejoras de mañana.

https://www.tinkercad.com/things/dMk13UTBKw6-kit-pasta-v1/edit?sharecode=vrinQFE6jyPGplYVddgVSiNfd78F_yfVECggdZEEI2I


***
