---
hide:
    - toc
---


# Idea y Concepto

El proyecto que me he propuesto desarrollar tiene como cometido solucionar un problema que me he encontrado en los puertos deportivos.
Los barcos anclados en los puertos no están del todo seguros ya que siempre se puede romper un cabo, una cadena, y el barco queda a la deriva pudiendo ocasionar daños a otros barcos, al puerto, o a si mismo. Es por esto que voy a crear un dispositivo inalámbrico e independiente de cualquier sistema del barco que nos permita tener información en tiempo real del estado del barco y generar alarmas dependiendo en la información de sus sensores.
