---
hide:
    - toc
---

# MT04 - Modelado 3D
## Introducción

El modelado 3D es un proceso en el cual se crea una representación en tres dimensiones de un objeto. Para crear este modelo, una persona "modela" el mismo. Para esto existen distintas técnicas como ser:
- Modelado de curvas o Nurbs
- Escultura digital
- Modelado 3d de bordes

Y los modelos son utilizados en una variedad casi infinita de lugares:
- Industria de la tv y cine
- Mecánica industrial y automotriz
- Ingeniería
- Arte

Hay muchos programas de modelado 3d, cada uno con sus características y enfocado en distintas formas de modelar y presentar un modelo, acá algunos de ellos:

### Fusion 360
Fusion 360 es un software CAD, CAM y de circuitos impresos de modelado 3D basado en la nube para el diseño y la manufactura de productos, creado por Autodesk.
Permite modelar en 3d, realizar ensambles y pruebas de manufactura entre otros.
Con compatibilidad para Windows y Mac. Una lástima que no pueda ser utilizado en Linux.
Tiene version de prueba de 30 días y luego hay que adquirir la licencia o se dispone de una licencia de estudiante gratuita.

Mucha información, videotutoriales y tips en: https://www.youtube.com/channel/UCiMwMz3RMbW5mbx0iDcRQ2g

### Blender
Blender es un software de código abierto, utilizado para modelar en 3d que funciona tanto en Wondows, como MacOS y Linux.
Lo utilizan principalmente los artistas para modelar y se basa en el modelado 3d por mallas.
Tiene toda una gama de herramientas asociadas para poder llevar a cabo las mas variadas tareas, Modelado, Renderizado, Animación & Rigging, Edición de Video, VFX, Composición, Texturizado y muchos tipos de Simulaciones.

Mucha información, videotutoriales y tips en: https://www.youtube.com/c/BlenderGuruOfficial

### Freecad
FreeCAD permite la creación de dibujos 2D, que luego se pueden convertir en modelos 3D. El software es paramétrico, lo que significa que los elementos individuales y su relación con otros elementos se pueden ajustar. Ajustar las proporciones de estos elementos puede conducir a la creación de estructuras altamente complejas. Además, una característica clave de FreeCAD es que el usuario tiene acceso permanente a los bocetos originales.

Información, videotutoriales y tips en: https://www.youtube.com/playlist?list=PLmnz0JqIMEzWQV-3ce9tVB_LFH9a91YHf

### Openscad
OpenSCAD es un software CAD de modelado muy potente, gratuito y de código abierto que le permite crear modelos 3D precisos con solo unas pocas líneas de código.
Este software de diseño 3D no se centra en el diseño de modelado 3D, sino en la parte de CAD  que es el diseño asistido por computadora.
OpenSCAD se puede utilizar para Gnu Linux, Windows y Mac OsX. Además no utiliza el cursor para dibujar, sino  se debe escribir los comandos.
Los expertos recomiendan este software ya que es muy simple para diseñar hardware de código abierto y como herramienta en la parte de investigación y educación.

Información, videotutoriales y tips en: https://www.youtube.com/c/mathcodeprint


## Tarea
### Objetivo principal
"Evaluar y seleccionar Sofware 3D. Demostrar y describir los procesos utilizados en el modelado con software 3D."

### Objetivos específicos
Comprender el uso de los comandos de software y diseño 3D para la herramienta seleccionada.
Comprender los tipos de archivos 3D y sus propiedades (e.g. nurbs vs mesh modelling)
Manejar los pasos de implementación del proceso de diseño
Manejar herramientas de diseño paramétrico
Identificación de software y sus diferencias
Desafío
Diseña y modela en 3D un elemento que sea de utilidad para tu proyecto final en el software paramétrico FUSION 360. Documenta los distintos pasos del proceso en tu página web.

NOTA: Si bien este modulo no implica directamente la producción de una pieza, el modelo debe poder imprimirse en 3D. Por lo tanto las piezas deben tener un tamaño máximo de 20X20X20 cm.



##Mi diseño ---- TODO
En este apartado voy a mostrar como diseñé una caja para la electrónica de mi proyecto en Fusion360.


base - extrusion
paredes extrusion
agujeros extrusion
tapa
manija

impresion 3d de prueba
