---
hide:
    - toc
---

# MI01 - Emprendimiento
## Workshop

En el workshop llevado a cabo por **Fundación daVinci** se trabajaron herramientas como:

 - Lienzo de negocio
 - HCD (Human Centered Design)
 - UX Research

En el grupo asignado trabajamos con el proyecto "Uber escolar", app para solucionar el transporte de los niños al colegio ante eventuales imprevistos.

Vimos que era un problema real y bastante común entre los padres que deben llevar a sus niños a distintas actividades. En el día a día surgen imprevistos como reuniones de trabajo, rotura de vehículo, enfermedades, etc.

Este es el documento generado con la herramienta de Human Centered Design: [Documento](../files/HCD7.pdf){:target="_blank"}

![](../images/hcd.png){ width=70% }


Imágenes del Design Sprint con una primera aproximación a las posibles pantallas de la app. [Link al proyecto en Invision](../files/Invision/Niños+a+la+Escuela.html){:target="_blank"}

![](../files/Invision/untitled - Detalle Conductor_@2x.png){ width=30% }


#Entrega personal
En el siguiente pdf apliqué algunas de las herramientas aprendidas en el Workshop a mi proyecto personal.
El proyecto personal a trabajar durante el curso es desarrollar una plataforma que permita saber la posición de un barco cuando está amarrado y comunicarla inalámbricamente al dueño del mismo, de manera de poder saber si el barco está anclado en el lugar que debe o a la deriva.

[Link al PDF Gantt y Canvas](../files/canvasganttrdearmas.pdf){:target="_blank"}
