---
hide:
    - toc
---

#  MT03 - Corte Láser controlado por computadora
## Introducción

En este módulo se trabajaron las particularidades de el corte con laser controlado por computadora.

La investigación en el uso del laser se teoriza en la década de 1910 y en los años 60 se desarrolla el laser de de dióxido de carbono, lo cual permite generar un laser con un costo muy inferior y una eficiencia mucho mayor que el anteriormente utilizado laser de rubí.

>LASER, viene dado por las siglas en inglás 'Light Amplification by Stimulated Emission of Radiation' o en español "amplificación de luz por emisión estimulada de radiación".

El mecanizado con laser es un proceso no convencional de índole térmica (basicamente es un incendio controlado) el cual no gener viruta, presenta una mejor precisión y acabado superficial.
Para el uso de la máquina se utilizan archivos vectoriales, los cuales pueden ser cortados, grabados completos o grabados lineales.
Para controlar la misma se puede utilizar el software [RDWorks](https://rdworkslab.com/){:target="_blank"}
 o [Lightburn](https://lightburnsoftware.com/){:target="_blank"}.</br>

 ![](../images/MT03/rdworks.webp){ width=300px }
 ![](../images/MT03/lightburn.jpeg){ width=300px } </br>

La máquina disponible para el uso en UTEC es una máquina genérica de CO2 con un área de trabajo de 90cm x 60cm.</br>


La consigna para este módulo era la siguiente:
 > "Diseñar un objeto para ser fabricado en máquina láser.
Material a utilizar: MDF de 3mm de espesor. El objeto debe tener como mínimo 3 piezas que lo compongan.  Las piezas se deben de poder ensamblar mediante encastres (sin la utilización de pegamento o fijaciones externas). El objeto a fabricar, debe de contener  las 3 operaciones básicas de la máquina láser (grabado raster, marcado sobre vector y corte sobre vector). El objeto a diseñar debe aplicar en alguna de sus partes, la técnica de curvado de madera (kerf bending). Tener en cuenta que la cantidad total de material a utilizar por estudiante será de 600 x 450 mm. Se cortarán las piezas diseñadas en un sólo archivo (formato .dxf) que entren dentro de esas dimensiones de material. Documentar el proceso y publicarlo en el respositorio Gitlab."

Para este módulo decidí diseñar una lámpara.
Este diseño contempla todas las partes de la consigna. Es un objeto diseñado para ser cortado en mdf de 3mm de espesor, está compuesto por 4 piezas, las mismas están unidas entre si por un encastre tipo snap-on.
Las piezas contemplan las 3 operaciones básicas de la cortadora laser, el corte vectorial, el grabado y el marcado sobre vector.
En la pieza delantera de la lámpara tiene 2 lugares donde se utiliza la técnica de kerfing para poder curvar el mdf. Este diseño puede ser cortado dentro de un material de 600mm x 450mm.

# Archivos decargables
[Archivo PDF v1.0](../files/MT03/cortelaserv10.pdf){:target="_blank"}

[Archivo DXF v1.0](../files/MT03/cortelaserv10.dxf){:target="_blank"}

# Imagenes del diseño vectorial v1.0
![](../images/MT03/completo1.png){ width=70% }

Imagen del diseño completo.

![](../images/MT03/kerfing1.png){ width=70% }

Patrón de kerfing y grabado.

![](../images/MT03/marcado1.png){ width=70% }

Detalle del marcado sobre vector.

# Fotos del corte v1.0
![](../images/MT03/foto1.jpeg){ width=40% }
![](../images/MT03/foto2.jpeg){ width=40% }
![](../images/MT03/foto3.jpeg){ width=40% }
![](../images/MT03/foto4.jpeg){ width=40% }


# Aspectos a mejorar para la versión v1.1
 - Kerfing, el kerfing utilizado se quiebra ya que el arco que tiene que flexar es muy cerrado.
 - Conectores Snap-on se quiebran con facilidad, hay que agregar círculos que permitan que el mdf flexe un poco mas.
 - Eliminación de lineas dobles.
 - Unificado de colores.

 ![](../images/MT03/foto1roto.jpeg){ width=40% }
 ![](../images/MT03/foto2roto.jpeg){ width=40% }
 ![](../images/MT03/foto3roto.jpeg){ width=40% }


 [Archivo PDF v1.1](../files/MT03/cortelaserv11.pdf){:target="_blank"}

 [Archivo DXF v1.1](../files/MT03/cortelaserv11.dxf){:target="_blank"}


# Miro generado en el MT03
 - **¿Cuál es la diferencia entre CAD y CAM?**

    CAD es Computer Aided Design, osea, diseño asistido por computadora.
    Es el uso de computadoras para diseñar, optimizar, mejorar, o analizar un diseño.
    CAM es Computer Aided Manufacturing, osea, fabricación asistida por computadora.
    Es el uso de computadoras para controlar las máquinas. El CAM toma la información generada por el CAD y en base a esto mueve las diferentes herramientas de la maquinaria para poder fabricar el diseño pedido.

 - **¿Qué tengo que tener en cuenta en el diseño del archivo para corte?**

    El archivo a utilizar en el corte laser o las maquinas 2D debe ser un archivo vectorial. Líneas vectoriales y puntos para el corte. Lineas vectoriales que componen formas cerradas para poder grabar. El archivo vectorial debe ser preferentemente con extensión dxf o en su defecto pdf vectorial.
    Debo tener en cuenta que cada máquina utiliza diferentes colores para las diferentes acciones, podemos utilizar tantos colores como acciones distintas necesitemos. Por ejemplo: linea roja para corte, linea negra para grabado vectorial, color verde para marcado vectorial.
    Tambipen debo tener en cuenta el espesor de los materiales, la tolerancia y los errores en los materiales con los que voy a trabajar si deseo realizar encastres.

 - **¿Qué materiales se pueden cortar con láser?**

    Los materiales a cortar dependen del tipo de laser.
    Con laser de CO2 dentro de los mas comunes: MDF, plywood, acrilico, policarbonato, cuero, corcho, goma eva, cartón, papel, piedra (grabado), vidrio (grabado).
    Con laser de fibra podemos agregar a los materiales arriba descritos, materiales metálicos.

    [Documento con información sobre materiales a cortar](https://cpl.org/wp-content/uploads/NEVER-CUT-THESE-MATERIALS.pdf){:target="_blank"}

 - **Describa el corte para estos seteos de máquina**

    - Power 20 Speed 200 :
      Corte con poca potencia y mucha velocidad, dependiendo del material va a cortar (papel) o marcar (mdf de 3mm)
    - Power 60 Speed 30 :
      Corte con mucha potencia y poca velocidad, dependiendo del material va a cortar (mdf 3mm) o marcar (mdf de 15mm)
    - Power 90 Speed 8 :
      Corte con potencia máxima y muy poca velocidad, dependiendo del material va a cortar (mdf 9 mm)
    - Power 75 Speed 15 :
      Corte con mucha potencia y poca velocidad, dependiendo del material va a cortar (mdf 6 mm)
    - Power 90 Speed 200 :
      Corte con potencia máxima y mucha velocidad, dependiendo del material va a marcar (mdf 3 mm)
    - Power 0 Speed 100 :
      No va a cortar ya que la potencia es 0 y no va a lograr disparar el laser.


![](../images/miromt03.png)
